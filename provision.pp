package { 'python':
	ensure => latest,
}

package { 'python-flask':
	ensure => latest,
}

exec { 'kill python processes':
	command => '/usr/bin/pkill -9 python',
}

exec { 'run hello world':
	command => '/usr/bin/python hello.py &',
	require => [ Exec['kill python processes'], Package['python'], Package['python-flask'] ],
}
